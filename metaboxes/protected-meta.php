<div class="my_meta_control">

	<label><strong>Protect Post/Page?</strong> (Optional)</label><br/>
	<p>
		<?php
        $clients = array('yes','no');
        foreach ($clients as $i => $client):
            $mb->the_field('protext_check');
            ?>
            <input type="radio" name="<?php $mb->the_name(); ?>" value="<?php echo $client; ?>"<?php if(!$mb->get_the_value('protext_check') and $client == 'no') echo 'checked="checked"'; else $mb->the_radio_state($client); ?>/> <?php echo ucfirst($client); ?>
        <?php endforeach; ?>
	</p>
	<label for="<?php $mb->the_name(); ?>"><strong>Protected Page Alternate Content</strong> (if nothing is entered the excerpt is used)</label>
    <p>Enter alternate text to be seen when a non-registered user tries to view the resource post.</p>
    <p>
        <?php
        $mb->the_field('alt_text');
		$settings = array(
                'quicktags' => array(
                        'buttons' => 'em,strong,link',
                ),
                'quicktags' => true,
                'tinymce' => true,
                'media_buttons'	=> false,
                'textarea_name'	=> $mb->get_the_name(),
                'textarea_rows'	=> 10,
                'teeny'			=> true
        );
        wp_editor(html_entity_decode($mb->get_the_value()), $mb->get_the_name(), $settings);
        ?>
    </p>

    <label for="<?php $mb->the_name(); ?>"><strong>Protected Page Alternate Excerpt</strong> (if nothing, default protected post message will be used)</label>
    <p>Enter alternate text to be seen when a non-registered user view the post excerpt.</p>
    <p>
    	<?php
		$mb->the_field('alt_excerpt');
		$excerpt_settings = array(
                'quicktags' => array(
                        'buttons' => 'em,strong,link',
                ),
                'quicktags' => true,
                'tinymce' => true,
                'media_buttons'	=> false,
                'textarea_name'	=> $mb->get_the_name(),
                'textarea_rows'	=> 5,
                'teeny'			=> true
        );
		wp_editor(html_entity_decode($mb->get_the_value()), $mb->get_the_name(), $excerpt_settings);
		?>
    </p>

</div>