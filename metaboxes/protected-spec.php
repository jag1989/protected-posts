<?php

$pp_metabox = new WPAlchemy_MetaBox(array
(

	'id' => '_protected_posts_meta',
	'title' => 'Protected Page',
	'context' => 'normal', //The part of the page where the edit screen section should be shown ('normal', 'advanced', or 'side'). 
	'priority' => 'default', //The priority within the context where the boxes should show ('high', 'core', 'default' or 'low') 
	'mode' => WPALCHEMY_MODE_EXTRACT,
	'prefix' => '_rev_',
	'template' => plugin_dir_path( __FILE__ ) . 'protected-meta.php'
));

/* eof */