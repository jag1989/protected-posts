<?php
/**
 * Plugin Name: Protected Posts
 * Description: Add restricted content to your blog posts with the use of an on-page form.
 * Version: 1.3
 * Author: Jonathan Garrett
 * Author URI: http://www.falkondigital.com
 * License: GPL2
 */

if(!class_exists('WPAlchemy_MetaBox')){
    include_once plugin_dir_path( __FILE__ ) . 'wpalchemy/metabox.php';
}
include_once plugin_dir_path( __FILE__ ) . 'metaboxes/protected-spec.php';
include_once plugin_dir_path( __FILE__ ) . 'domainlookup/effectiveTLDs.inc.php';
include_once plugin_dir_path( __FILE__ ) . 'domainlookup/regDomain.inc.php';

if (is_admin()) wp_enqueue_style('wpalchemy-metabox', plugins_url('metaboxes/meta.css', __FILE__));

function pp_enqueue(){
	global $wp_styles;
	wp_enqueue_style('pp_styles', plugins_url('pp_styles.css', __FILE__));
	wp_enqueue_style('pp_styles_ie', plugins_url('pp_styles_ie.css', __FILE__));
	$wp_styles->add_data('pp_styles_ie', 'conditional', 'IE 7' );
	wp_enqueue_script('jquery');
	wp_enqueue_script('pp_placeholders', plugins_url('js/placeholders.js', __FILE__), '', '', true);
	//wp_enqueue_script('pp_scripts', plugins_url('js/pp_scripts.js', __FILE__));

}
add_action('wp_print_styles', 'pp_enqueue', 9);

define('DOMAIN_STR', str_replace(' ', '_', get_bloginfo('name')));
//define('DOMAIN_NAME', get_site_url());

function pp_install(){
	global $wpdb;
   	$table_name = $wpdb->prefix . "pp_users";

   	$sql = "CREATE TABLE $table_name (
  			ID int(11) NOT NULL AUTO_INCREMENT,
			email VARCHAR(255) NOT NULL,
			name VARCHAR(255) NOT NULL,
			phone VARCHAR(20),
			UNIQUE KEY (ID)
    	);";

   require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
   dbDelta( $sql );

}
register_activation_hook( __FILE__, 'pp_install' );


function pp_encode($string,$key) {
	$key 	= sha1($key);
	$strLen = strlen($string);
	$keyLen = strlen($key);
	for ($i = 0; $i < $strLen; $i++) {
		$ordStr = ord(substr($string,$i,1));
		if ($j == $keyLen) { $j = 0; }
		$ordKey = ord(substr($key,$j,1));
		$j++;
		$hash .= strrev(base_convert(dechex($ordStr + $ordKey),16,36));
	}
	return $hash;
}

function pp_decode($string,$key) {
	$key 	= sha1($key);
	$strLen = strlen($string);
	$keyLen = strlen($key);
	for ($i = 0; $i < $strLen; $i+=2) {
		$ordStr = hexdec(base_convert(strrev(substr($string,$i,2)),36,16));
		if ($j == $keyLen) { $j = 0; }
		$ordKey = ord(substr($key,$j,1));
		$j++;
		$hash .= chr($ordStr - $ordKey);
	}
	return $hash;
}

/* Set access cookie for resource access */
function pp_setRestrictedCookie($postEmail,$postRedirect){
	setcookie(DOMAIN_STR."_restricted_content", pp_encode($postEmail,wp_salt()), time()+60*60*24*365, "/");
	header("Location: $postRedirect", true, 302);
	exit;
}

function pp_check_post(){
	global $userarray; $userarray=array();
	global $wpdb;
	$userEmailExists 	= true;
	$userDataExists 	= true;

	if(isset($_POST['submituser'])){
		$postName 		= sanitize_text_field($_POST['userName']);
		$postEmail 		= sanitize_text_field($_POST['useremail']);
		$contactnum 	= sanitize_text_field( $_POST['contactnum']);
		$postRedirect 	= sanitize_text_field($_POST['pp_postID']);
		$tempEmail 		= is_email($_POST['useremail']);
		$validData 		= true;

		if($postName=='')$validData = false;

		if($tempEmail && $validData){

			$post 				= array();
			$post['email']		= $postEmail;
			$post['userName']	= $postName;

			$table		= $wpdb->prefix.'pp_users';
			$data		= array( 'email' => $postEmail,
								 'name' => $postName,
								 'phone' => $contactnum );

			if($wpdb->insert( $table, $data )){
                $adminemail = get_bloginfo('admin_email');
				$to = $adminemail;
				$subject = get_bloginfo( 'name' )." Restricted Content - New User";
				$message = "<html><body style='font-family: Arial, sans-serif;font-size:12px;'><strong>".$postName."</strong> has accessed restricted content on ".get_bloginfo( 'name' )."<br /><br />";
				$message .="Email Address: <strong>".$postEmail. "</strong><br />";
				$message .="Phone Number: <strong>".$contactnum. "</strong><br />";
				$message .="</body></html>";
				$headers = "From: ".get_bloginfo( 'name' )." ".$adminemail." \r\n";
				$headers .="Reply-To: ".get_bloginfo( 'name' )." ".$adminemail." \r\n";
				$headers .= "MIME-Version: 1.0\r\n";
				$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
				$headers .='X-Mailer: PHP/' . phpversion();

                pp_setRestrictedCookie($postEmail,get_permalink($postRedirect).'?access=true');

                @mail($to, $subject, $message, $headers);
                @mail('jonathan@falkondigital.com', $subject.' - BCC ', $message, $headers);

			}
		} else{ $userDataExists = false; }
	}elseif(isset($_POST['submitemail'])){
		if(is_email($_POST['useremail2'])){
			$postEmail 		= sanitize_text_field($_POST['useremail2']);
			$postRedirect 	= sanitize_text_field($_POST['pp_postID']);
			$table_name 	= $wpdb->prefix.'pp_users';
			$mylink 		= $wpdb->get_row("SELECT * FROM $table_name WHERE email = '$postEmail'", ARRAY_A);

			($mylink != null) ? pp_setRestrictedCookie($postEmail,get_permalink($postRedirect).'?access=true') : $userEmailExists = false;
		} else{ $userEmailExists = false; }
	}else{
		$postName 		= '';
		$postEmail 		= '';
		$contactnum 	= '';
	}

	$userarray=array( 	'email_exists' 	=> $userEmailExists,
						'data_exists' 	=> $userDataExists,
						'name'			=> $postName,
						'email' 		=> $postEmail,
						'number' 		=> $contactnum
					);

}
add_action('init', 'pp_check_post');

function pp_add_form($content){
	$output	= $content;
	if(is_single()):
		global $userarray;
		global $pp_metabox;
		global $wpdb;
		$userEmailExists 		= $userarray['email_exists'];
		$userDataExists 		= $userarray['data_exists'];
		$submittedName			= $userarray['name'];
		$submittedEmail			= $userarray['email'];
		$submittedNumber		= $userarray['number'];
		$ppMeta 				= $pp_metabox->the_meta();
		$allowRestrictedPost 	= false;
        $allowuserIn 	        = false;
        $output					= '';

		if(!isset($ppMeta['protext_check']) || $ppMeta['protext_check'] == 'no') $allowRestrictedPost = true;
		$setcookieValue = (isset($_COOKIE[DOMAIN_STR."_restricted_content"])) ? $_COOKIE[DOMAIN_STR."_restricted_content"] : 'no_cookie';
		$setCookieDecode = pp_decode($setcookieValue, wp_salt());

		$table_name 	= $wpdb->prefix.'pp_users';
		$mylink 		= $wpdb->get_row("SELECT * FROM $table_name WHERE email = '$setCookieDecode'", ARRAY_A);
		if ($mylink != null) $allowuserIn = true;

		if(isset($_GET['access']) && $_GET['access']=='true'){
            $output .= '<span class="pp_success">Thank you. You now have access to the restricted content.</span>';
            $allowuserIn = true;
        }

		if ( is_user_logged_in() || $allowuserIn || $allowRestrictedPost) {

		  		$output .= $content;
		  		$output .= wp_link_pages( array( 'before' => '<nav>' . __( 'Pages:' ), 'after' => '</nav>', 'echo' => 0 ) );

	   	} else {

			$altText = $ppMeta['alt_text'];
		   	if($ppMeta['alt_text'] == "") $altText = wp_trim_words($content);

		   		$output .= wpautop($altText);
           		$output .= '
		   		<div id="protected-post" class="noaccess">
			  		<h3 class="restricted-title">Please Register To View This Content</h3>
			  		<p>Please fill in our short form below. Don’t worry, we won’t spam you or share your details.</p>

                      <form action="" method="post" class="formrestrict" id="pp_register" >'; ?>
                       <?php if(!$userDataExists) $output .= '<span class="pp_error">The details you have provided are not correct. Please check all fields and submit them again.</span>';?>
                          <?php $output .= '
                          <div class="row">
                            <p class="pp_left"><input type="text" name="userName" id="userName" placeholder="Your Name*" value="'.$submittedName.'" /></p>
                            <p class="pp_left right"><input type="text" name="useremail" id="useremail" placeholder="Your Email Address *" value="'.$submittedEmail.'" /></p>
                          </div>
                          <div class="row">
                            <p class="pp_left"><input type="text" name="contactnum" id="contactnum" placeholder="Contact Number" value="'. $submittedNumber.'" /></p>
                            <p class="pp_left right"><button type="submit" name="submituser" class="button" value="Register & View" ><span>Register & View</span></button></p>
                          </div>
                          <input type="hidden" name="pp_postID" value="'. get_the_ID().'" />
                      </form>

			  		<div class="clear"></div>
			  		<div class="divide"></div>

			  		<p>If you have already registered your with us, please enter your provided email address.</p>
                      <form action="" method="post" class="formrestrict" id="pp_login" >' ;?>
                      <?php if(!$userEmailExists) $output .= '<span class="pp_error">The email address supplied is not recognised in our records. Please try again or register using the form above.</span>';?>
                        <?php $output .= '
                        <div class="row">
                          <p class="pp_left"><input type="text" name="useremail2" id="useremail2" placeholder="Your Email address *" /></p>
                          <p class="pp_left right"><button type="submit" name="submitemail" class="button" value="View now" ><span>View Now</span></button></p>
                        </div>
                        <input type="hidden" name="pp_postID" value="'.get_the_ID().'" />
                      </form>

			  		<div class="clear"></div>

		  </div>';

	   }
	endif;
	return $output;

}
add_filter( 'the_content', 'pp_add_form', 20);

function pp_the_excerpt($excerpt){
	global $pp_metabox;
	global $wpdb;
	$ppMeta 				= $pp_metabox->the_meta();
	$allowRestrictedPost 	= false;
	$output					= '';

	if(!isset($ppMeta['protext_check']) || $ppMeta['protext_check'] == 'no') $allowRestrictedPost = true;

	$setcookieValue = (isset($_COOKIE[DOMAIN_STR."_restricted_content"])) ? $_COOKIE[DOMAIN_STR."_restricted_content"] : 'no_cookie';
	$setCookieDecode = pp_decode($setcookieValue, wp_salt());

	$table_name 	= $wpdb->prefix.'pp_users';
	$mylink 		= $wpdb->get_row("SELECT * FROM $table_name WHERE email = '$setCookieDecode'", ARRAY_A);
	$allowuserIn 	= false;
	if ($mylink != null) $allowuserIn = true;

	if ( is_user_logged_in() || $allowuserIn || $allowRestrictedPost) {

		$output .= wpautop($excerpt);

	} else {

		$altExcerpt = wpautop($ppMeta['alt_excerpt']);
		if($ppMeta['alt_excerpt'] == "") $altExcerpt = 'This post is protected, if you wish to view the full content please login.';
		$output .= $altExcerpt;
   }
	return $output;
}
add_filter( 'the_excerpt', 'pp_the_excerpt' )
?>