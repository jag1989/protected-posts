== Changelog ==

= 1.3 =
* Added additional styling
* Removed deprecated mysql_escape_string references
* Removed self domain check for WP function
* Removed domain from cookie
* Suppress mail errors (for machines without mail server)

= 1.2 =
* Original Plugin
